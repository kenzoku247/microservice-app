import {useState, useEffect} from 'react'
import axios from 'axios'


function UserAPI(token) {
    // const [name, setname] = useState('')
    // const [email, setEmail] = useState('')
    // const [phoneNumber, setphoneNumber] = useState([])
    // const [storeLocation, setstoreLocation] = useState('')
    // const [storeName, setstoreName] = useState('')
    const [isLogged, setIsLogged] = useState(false)
    const [isAdmin, setIsAdmin] = useState(false)

    useEffect(() =>{
        if(token){
            const getUser = async () =>{
                try {
                    const res = await axios.get(`/api_user/info`, {
                        headers: 
                        {
                            'Authorization': token
                        }
                    })
                    
                    setIsLogged(true)
                    const user = res.data.user
                    user.role === 1 ? setIsAdmin(true) : setIsAdmin(false)
                    // setname(user.name)
                    // setEmail(user.email)
                    // setphoneNumber(user.phoneNumber)
                    // setstoreLocation(user.storeLocation)
                    // const listLocation = storeLocation !== "" ? storeLocation.split(',') : ["","",""]
   
                    // setstoreName(res.data.user.storeName)
                } catch (err) {
                    alert(err.response.data.msg)
                }
            }

            getUser()
            
        }
    },[token])

    return {
        isLogged: [isLogged, setIsLogged],
        isAdmin: [isAdmin, setIsAdmin],
        // name: [name, setname],
        // email: [email, setEmail],
        // phoneNumber: [phoneNumber, setphoneNumber],
        // storeLocation: [storeLocation, setstoreLocation],
        // storeName: [storeName, setstoreName],
    }
}

export default UserAPI
 