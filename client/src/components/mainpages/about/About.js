import React, { useState, useContext, useEffect, useRef } from 'react'
import { GlobalState } from '../../../GlobalState'
import { FaPlus } from "react-icons/fa6";
import provinces from "../../../api/Provinces.json";
import axios from 'axios'

const About = () => {
  const state = useContext(GlobalState)
  const [name, setName] = state.userAPI.name
  const [email, setEmail] = state.userAPI.email
  const [phoneNumber, setphoneNumber] = state.userAPI.phoneNumber
  const [storeLocation, setstoreLocation] = state.userAPI.storeLocation
  const [storeName, setstoreName] = state.userAPI.storeName
  const [newStoreName, setNewStoreName] = useState('')
  const listLocation = storeLocation !== "" ? storeLocation.split(',') : ["","",""]
  const [selectedProvinceCodename, setselectedProvinceCodename] = useState(listLocation[0])
  const [selectedDistrictCodename, setselectedDistrictCodename] = useState(listLocation[1])
  const [selectedWardCodename, setselectedWardCodename] = useState(listLocation[2])
  const [selectedProvinceName, setselectedProvinceName] = useState("")
  const [selectedDistrictName, setselectedDistrictName] = useState("")
  const [selectedWardName, setselectedWardName] = useState("")
  const [listProvinces, setlistProvinces] = useState([])
  const [listDistricts, setListDistricts] = useState([])
  const [listWards, setlistWards] = useState([])
  const [editStore, seteditStore] = useState(false)

  useEffect(() => {
    const getName = async () => {
      const res1 = await axios.post('/api_user/location', { province: "", district: ""})
      setselectedProvinceName(res1.data.provinces.find(province => province.codename === selectedProvinceCodename).name)
      const res2 = await axios.post('/api_user/location', { province: selectedProvinceCodename, district: ""})
      setselectedDistrictName(res2.data.provinces.find(district => district.codename === selectedDistrictCodename).name)
      const res3 = await axios.post('/api_user/location', { province: selectedProvinceCodename, district: selectedDistrictCodename})
      setselectedWardName(res3.data.provinces.find(ward => ward.codename === selectedWardCodename).name)
    }
    if (storeLocation !== "") {
      if (!editStore) {
        getName()
      }
    } else {
      setselectedProvinceName("---")
      setselectedDistrictName("---")
      setselectedWardName("---")
    }
  },[editStore,selectedProvinceCodename,selectedDistrictCodename,selectedWardCodename])

  // console.log(selectedProvinceName)

  useEffect(() => {
    const getListProvinces = async () => {
      const res = await axios.post('/api_user/location', { province: "", district: ""})
      setlistProvinces(res.data.provinces)
      // if (selectedProvinceCodename === "") {
      //   setselectedProvinceCodename(res.data.provinces[0].codename)
      // }
    }
    const getListDistricts = async () => {
      const res = await axios.post('/api_user/location', { province: selectedProvinceCodename, district: ""})
      setListDistricts(res.data.provinces)
      // if (selectedDistrictCodename === "") {
      //   setselectedDistrictCodename(res.data.provinces[0].codename)
      // }
    }
    const getListWards = async () => {
      const res = await axios.post('/api_user/location', { province: selectedProvinceCodename, district: selectedDistrictCodename})
      setlistWards(res.data.provinces)
      // if (selectedWardCodename === "") {
      //   setselectedWardCodename(res.data.provinces[0].codename)
      // }
    }
    // if (editStore) {
      // if (selectedProvinceCodename === "" && selectedDistrictCodename === "" && selectedWardCodename === "") {
        getListProvinces()
    //     console.log(selectedProvinceCodename)
    //   } else if (selectedProvinceCodename !== "" && selectedDistrictCodename === "" && selectedWardCodename === "") {
    //     getListProvinces()
    //     getListDistricts()
    //   } else if (selectedProvinceCodename !== "" && selectedDistrictCodename !== "" && selectedWardCodename === ""){
    //     getListProvinces()
    //     getListDistricts()
    //     getListWards()
    //   } else if (selectedProvinceCodename !== "" && selectedDistrictCodename !== "" && selectedWardCodename !== ""){
    //     getListProvinces()
    //     getListDistricts()
    //     getListWards()
      // }

    // }
  },[selectedProvinceCodename,selectedDistrictCodename,selectedWardCodename])

  // console.log(listProvinces)

  const onClickProvinces = async () => {
    const res = await axios.post('/api_user/location', { province: "", district: ""})
    setlistProvinces(res.data.provinces)
    // if (selectedProvinceCodename === "") {
    //   setselectedProvinceCodename(res.data.provinces[0].codename)
    // }
  }

  const onBLurProvinces = () => {
    setlistProvinces([])
  }

  const handleSelectedProvince = async e => {
    setselectedProvinceCodename(e.target.value)
    const res1 = await axios.post('/api_user/location', { province: e.target.value, district: ""})
    setListDistricts(res1.data.provinces)
  }

  const handleSelectedDistrict = async e => {
    setselectedDistrictCodename(e.target.value)
    const res = await axios.post('/api_user/location', { province: selectedProvinceCodename, district: e.target.value})
    setlistWards(res.data.provinces)
  }

  const handleSelectedWard = e => {
    setselectedWardCodename(e.target.value)
  }

  const handleEditStoreName = e => {
    setNewStoreName(e.target.value)
  }

  const handleStoreEdit = async () => {
    seteditStore(true)
  }

  const handleSaveStoreEdit = async () => {
    const newStoreLocation = [selectedProvinceCodename,selectedDistrictCodename,selectedWardCodename].join(',')
    await axios.put('/api_user/update-store', {email:email,storeName:storeName,storeLocation:newStoreLocation})
    setstoreLocation(newStoreLocation)
    seteditStore(false)
  }

  const handleCancelStoreEdit = () => {
    setselectedProvinceCodename(listLocation[0])
    setselectedDistrictCodename(listLocation[1])
    setselectedWardCodename(listLocation[2])
    // setlistProvinces([])
    // setListDistricts([])
    // setlistWards([])
    seteditStore(false)
  }

  var inputProvince = document.getElementById("selectProvince");
  var ulProvinces = document.getElementById("listProvinces");

  function setProvinceInput(event) {
    if (event.target.tagName === "LI") {
      setselectedProvinceName(event.target.textContent)
    }
    ulProvinces.style.display = "none";

  }
  
  if (inputProvince, ulProvinces){
    document.addEventListener("click", function(event) {
      console.log(event.target)
      if (event.target !== inputProvince) {
        if (!ulProvinces.contains(event.target)) {
          ulProvinces.style.display = "none";
        }
      } 
    });
  }

  function hideListPronvinces(event) {
    event.stopPropagation()
  }
  function showUpListProvinces() {
    ulProvinces.style.display = "block";
  }



  return (
    <div className='profile' onClick={hideListPronvinces}>
      <div className='store_info'>
        <h3>Thông tin cửa hàng</h3>

        <div className='store_info_name'>
          <label>
            <span>Tên cửa hàng</span>
          </label>
          <input type='text' value={!editStore ? newStoreName : storeName} disabled={!editStore} onChange={handleEditStoreName}></input>
        </div>
        <div className='store_info_location' >
          <div className='province' >
            <label>
              <span>Tỉnh/Thành phố</span>
            </label>
            
            <input value={selectedProvinceName} id='selectProvince' disabled={!editStore} onFocus={showUpListProvinces}></input>
            
            {editStore && 
              <ul id='listProvinces' style={{'display':'none'}} >
                {listProvinces.map(province => 
                  <li onClick={setProvinceInput}>{province.name}</li>  
                )}
              </ul>
            }
          </div>

          <div className='district'>
            <label>
              <span>Quận/Huyện</span>
            </label>
            {
              !editStore ?
              <input value={selectedDistrictName}  disabled={!editStore}></input> :
            <select value={selectedDistrictCodename} onChange={handleSelectedDistrict} disabled={!editStore}>
              {listDistricts.map(district => 
                <option value={district.codename} selected={district.codename === selectedDistrictCodename && 'selected'}>{district.name}</option>
              )} 
            </select>
            }
          </div>

          <div className='ward'>
            <label>
              <span>Xã/Phường</span>
            </label>
            {
              !editStore ?
              <input value={selectedWardName}  disabled={!editStore}></input> :
            <select value={selectedWardCodename} onChange={handleSelectedWard} disabled={!editStore}>
            {listWards.map(ward => 
              <option value={ward.codename} selected={ward.codename === selectedWardCodename && 'selected'}>{ward.name}</option>
            )}
            </select>
            }
          </div>
        </div>
        <div className='store_info_edit'>
          {!editStore 
          ? <button onClick={handleStoreEdit}>
                Chỉnh sửa thông tin cửa hàng
            </button>
          : <>
              <button onClick={handleSaveStoreEdit}>
                Lưu
              </button>
              <button onClick={handleCancelStoreEdit}>
                Huỷ
              </button>
            </>
          }
          
        </div>
      </div>
      {/* <div className='contact_info'>
        <h3>Thông tin người dùng</h3>
        <label>
          <span>Tên người dùng</span>
        </label>
        <input type='text' value={name}></input>
        <label>
          <span>Địa chỉ email</span>
        </label>
        <input type='text' value={email}></input>
        <label>
          <span>Số điện thoại</span>

        {phoneNumber.map(el => (
          <input type='text' value={el}></input>
        ))}
        <FaPlus />
        </label>
      </div> */}
      {/* <div className='security'>
          <h3>Đổi mật khẩu</h3>
          <label>
            <span>Mật khẩu cũ</span>
          </label>
          <input type='text'></input>
          <label>
            <span>Mật khẩu mới</span>
          </label>
          <input type='text'></input>
          <label>
            <span>Nhập lại mật khẩu mới</span>
          </label>
          <input type='text'></input>
      </div> */}
    </div>
  )
}

export default About