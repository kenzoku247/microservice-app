import React, { useContext, useEffect, useState } from 'react'
import { GlobalState } from '../../../GlobalState'
import Chart from "react-apexcharts";

const Statistical = () => {
  const state = useContext(GlobalState)
  const [allOrders, setAllOrders] = state.orderAPI.allOrders
  let firstDayToToday = []
  let orderCreated = []
  const today = new Date().getTime()

  if (allOrders.length > 0) {
    orderCreated = allOrders.map(order => new Date(order.datetime).getTime())
    let firstDayOrder = orderCreated[orderCreated.length - 1]
    firstDayToToday.push(firstDayOrder)
    do {
      firstDayOrder += 60 * 60 * 24 * 1000 * 1
      firstDayToToday.push(firstDayOrder)
    } while (firstDayOrder <= today);
  }


  let series = [
    {
      name: "Số đơn hàng",
      data:
        firstDayToToday.map(day =>
        ({
          x: day,
          y: orderCreated.filter(date => new Date(date).getTime() === new Date(day).getTime()).length
        })
        )
    }
  ]

  let maxCount = Math.max(...series[0].data.map(y => y.y))
  maxCount = 10 - maxCount % 10 + maxCount
  
  const options = {
    chart: {
      id: 'realtime',
      height: 350,
      type: 'area',
      animations: {
        enabled: true,
        easing: 'easeinout',
        speed: 800,
        animateGradually: {
            enabled: true,
            delay: 150
        },
        dynamicAnimation: {
            enabled: true,
            speed: 350
        }
      },
      stacked: false,
      toolbar: {
        autoSelected: 'zoom'
      },
      zoom: {
        type: 'x',
            enabled: true,
            autoScaleYaxis: true
      }
    },
    dataLabels: {
      enabled: false
    },
    title: {
      text: 'Realtime Chart',
      align: 'left'
    },
    markers: {
      size: 0
    },
    xaxis: {
      type: 'datetime',
    },
    yaxis: {
      max: maxCount,
      min: 0
    },
    legend: {
      show: false
    },
    fill: {
      type: 'gradient',
      gradient: {
        shadeIntensity: 1,
        inverseColors: false,
        opacityFrom: 0.5,
        opacityTo: 0,
        stops: [0, 90, 100]
      },
    },
  }

  return (
    <div className='statistical'>
      <div className='chart'>
        <Chart options={options} series={series} type="area" height={350} />
      </div>
    </div>
  )
}

export default Statistical