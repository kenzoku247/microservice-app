import React from "react";
import "./Header.css";
import axios from 'axios'


const Header = () => {
  const logoutUser = async () =>{
    await axios.get(`/api_user/logout`)
    localStorage.removeItem('firstLogin' )
    window.location.href = "/login";
}

  return (
    <div className="header">
      <a href="/" className="logo">
        <h2>Checking Order</h2>
      </a>
      <nav className="navbar">
        <ul>
          <li>
            <a href="/">Trang chủ</a>
          </li>
          <li>
            <a href="/import">Nhập</a>
          </li>
          <li>
            <a href="/returned">Hoàn</a>
          </li>
          <li>
            <a href="/statistical">Thống kê</a>
          </li>
          <li><a href="/login" onClick={logoutUser}>Đăng xuất</a></li>
        </ul>
      </nav>
    </div>
  );
};


export default Header