process.env.NODE_ENV = 'test';
process.env.UPLOADS_FOLDER_PATH = './test/'
const request = require('supertest')
const server = require('../server')
const app = request(server)

const { MongoClient } = require('mongodb')
const client = new MongoClient(process.env.MONGODB_URL);
const database = client.db("test");
const orders_coll = database.collection("orders");

const Prometheus = require('prom-client');
const register = new Prometheus.Registry();

describe('Testing order api:' , () => {
    beforeAll(async () => {
        await orders_coll.deleteMany({})
        const testData = [
            {
                orderCode: "TestOrderCode0",
                datetime: "1/1/1111",
                platform: "TestPlatform",
                status: "Sent",
                link: "TestLink",
                note: "TestNote"
            },
            {
                orderCode: "TestOrderCode1",
                datetime: "1/1/1111",
                platform: "TestPlatform",
                status: "Sending",
                link: "TestLink",
                note: "TestNote"
            },
            {
                orderCode: "TestOrderCode2",
                datetime: "1/1/1111",
                platform: "TestPlatform",
                status: "Returned",
                link: "TestLink",
                note: "TestNote"
            },
    
        ]
        orders_coll.insertMany(testData, { ordered: true })
    });


    describe('/getOrders', () => {
        it('it should GET all the orders', async () => {
            const res = await app
            .get('/api_order/orders')
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(true)
        })
    })

    describe('/getAllOrders', () => {
        it('it should GET all the orders', async () => {
            const res = await app
            .get('/api_order/allOrders')
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(true)
        })
    })

    describe('/updateOrder', () => {
        it('it should update an order', async () => {
            let order = {
                orderCode: "TestOrderCode2",
                note: "Something",
                status: "Sending"
            }
            const res = await app
            .put('/api_order/order')
            .send(order)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(true)
        }, 10000)
    })
    describe('/updateOrders', () => {
        it('it should update many orders', async () => {
            let orderLists = {
                orderCodes: 
                [
                    "TestOrderCode0",
                    "TestOrderCode1",
                    "TestOrderCode2",
                ]
            }
            const res = await app
            .put('/api_order/orders')
            .send(orderLists)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(true)
        }, 20000)
    })

    describe('/importOrder', () => {
        it('it should import an order',async () => {
            let order = {
                orderCode: "TestOrderCode3"
            }
            const res = await app
            .post('/api_order/order')
            .send(order)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(true)
            expect(res.body.msg).toBe("Import an order successfully!")
        }, 10000)
        it('it should NOT post an order without orderCode field', async () => {
            let order = {}
            const res = await app
            .post('/api_order/order')
            .send(order)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(false)
            expect(res.body.msg).toBe("Thiếu trường orderCode. / Missing orderCode field.")
        })
        it('it should NOT post an order with empty orderCode', async () => {
            let order = {orderCode:""}
            const res = await app
            .post('/api_order/order')
            .send(order)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(false)
            expect(res.body.msg).toBe("Mã vận đơn không được để trống. / Empty order code.")
        })
        it('it should NOT post an exist order', async () => {
            let order = {orderCode:"TestOrderCode3"}
            const res = await app
            .post('/api_order/order')
            .send(order)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(false)
            expect(res.body.msg).toBe("Mã này đã bị trùng. / This code has been duplicated.")
        })
        it('it should NOT post an order with invalid orderCode', async () => {
            let order = {orderCode:"InvalidOrderCode"}
            const res = await app
            .post('/api_order/order')
            .send(order)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(false)
            expect(res.body.msg).toBe("Mã vận đơn không hợp lệ. / Invalid order code.")
        },60000)
    })


    describe('/uploadFile', () => {
        it('it should upload a file', async () => {
            const res = await app
            .post('/api_order/upload')
            .set('Content-Type', 'text/html')
            .attach('file', `${__dirname}/test.csv`)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(true)
            expect(res.body.msg).toBe("File is uploaded successfully!")
        })
        it('it should not upload a exist file ', async () => {
            const res = await app
            .post('/api_order/upload')
            .set('Content-Type', 'text/html')
            .attach('file', `${__dirname}/test.csv`)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(false)
            expect(res.body.msg).toBe("File be duplicated!")
        })

    },10000)

    describe('/metrics', () => {
        it('it should return Prometheus metrics', async () => {
            const metrics = await register.metrics();
            const res = await app
            .get('/api_order/metrics')
            .send(metrics)
            expect(res.statusCode).toBe(200);
            expect(res.headers['content-type'].split(';')[0]).toMatch(register.contentType.split(';')[0]);
        });
    });
    
    describe('/slow', () => {
        it('it should return Prometheus metrics for slow', async () => {
            const res = await app
            .get('/api_order/slow')
            expect(res.statusCode).toBe(200);
        },10000);
    });
    afterAll(async () => {
        await orders_coll.deleteMany({})
    })
})