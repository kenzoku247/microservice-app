const router = require('express').Router()
const userCtrl = require('../controller/userCtrl')
const auth = require('../middleware/auth')

router.post('/register', userCtrl.register)
router.post('/login', userCtrl.login)
router.get('/logout', userCtrl.logout)
router.get('/refresh_token', userCtrl.refreshToken)
router.get('/info', auth,  userCtrl.getUser)
// router.post('/location',userCtrl.location)
// router.put('/update-user',auth,userCtrl.updateUser)
// router.put('/update-store',userCtrl.updateStore)


router.route("/metrics")
    .get(userCtrl.getMetrics)

router.route("/slow")
    .get(userCtrl.slow)


module.exports = router