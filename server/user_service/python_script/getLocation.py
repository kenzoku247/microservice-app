import sys
import json

f = open("python_script/Provinces.json")
data_location = json.load(f)

def listProvince():
    data = [{"name": x["name"],"codename":x["codename"]} for x in data_location]
    return json.dumps(data)
def listDistrict(codename_province):
    data = [{"name": y["name"],"codename":y["codename"]} for x in data_location for y in x["districts"] if x["codename"] == codename_province]
    return json.dumps(data)
def listWard(codename_province, codename_district):
    data = [{"name": z["name"],"codename": z["codename"]} for x in data_location for y in x["districts"] for z in y["wards"] if x["codename"] == codename_province and y["codename"] == codename_district]
    return json.dumps(data)
if __name__ == "__main__":
    if len(sys.argv) == 1:
        print(listProvince())
    elif len(sys.argv) == 2:
        print(listDistrict(sys.argv[1]))
    elif len(sys.argv) == 3:
        print(listWard(sys.argv[1],sys.argv[2]))
sys.stdout.flush()