process.env.NODE_ENV = 'test';
const request = require('supertest')
const server = require('../server')
const auth = require('../middleware/auth')
const app = request(server)
const { MongoClient } = require('mongodb')
const client = new MongoClient(process.env.MONGODB_URL);
const database = client.db("test");
const users_coll = database.collection("users");

const Prometheus = require('prom-client');
const register = new Prometheus.Registry();

describe('Testing user api:' , () => {
    const userExisted = {
        name: "TestUsername",
        email: "TestEmailExisted",
        password: "TestPassword"
    }
    beforeAll(async () => {
        await users_coll.deleteMany({}, (err) => {
            console.log(err)
        })
        users_coll.insertOne(userExisted)
    });
    const user = {
        name: "TestUsername",
        email: "TestEmail",
        password: "TestPassword"
    }
    const userLowSecPass = {
        name: "TestUsername",
        email: "TestEmail1",
        password: "Test"
    }
    const userNotExist = {
        name: "TestUsername",
        email: "TestEmailNotExist",
        password: "TestPassword"
    }
    const userIncorrectPass = {
        name: "TestUsername",
        email: "TestEmail",
        password: "TestIncorrectPassword"
    }
    describe('/register', () => {
        it('it should register with user credential', async () => {
            const res = await app
            .post('/api_user/register')
            .send(user)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(true)
        })
        it('it should not register with an email existed', async () => {
            const res = await app
            .post('/api_user/register')
            .send(userExisted)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(false)
            expect(res.body.msg).toBe("This email already exists.")
        })
        it('it should not register with a low security password', async () => {
            const res = await app
            .post('/api_user/register')
            .send(userLowSecPass)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(false)
            expect(res.body.msg).toBe("Password is at least 6 characters long.")
        })
    })
    let token = ''
    let cookie = ''
    describe('/login', () => {
        it('it should not login with user not exist', async () => {
            const res = await app
            .post('/api_user/login')
            .send(userNotExist)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(false)
            expect(res.body.msg).toBe("User does not exist.")

        })
        it('it should not login with incorrect password', async () => {
            const res = await app
            .post('/api_user/login')
            .send(userIncorrectPass)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(false)
            expect(res.body.msg).toBe("Incorrect password.")
        })
        it('it should login with correct user credential', async () => {
            const res = await app
            .post('/api_user/login')
            .send(user)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(true)
            expect(res.body.msg).toBe("Logged in successfully.")
            console.log(res.headers['set-cookie'][0])
            cookie = res.headers['set-cookie'][0].split(';')[0]
            console.log(cookie)
        })
    })
    describe('/refreshToken', () => {
        it('it should get accessToken by using cookies', async () => {
            const res = await app
            .get('/api_user/refresh_token')
            .set('Cookie', cookie)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(true)
            token = res.body.accessToken
        })
        it('it should not get accessToken by not using cookies', async () => {
            const res = await app
            .get('/api_user/refresh_token')
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(false)
            expect(res.body.msg).toBe("Please Login or Register")
        })
    })
    describe('/getUser', () => {
        it('it should get info user by provide correct accessToken', async () => {
            const res = await app
            .get('/api_user/info')
            .set('Authorization', token)
            expect(res.statusCode).toBe(200);
            expect(res.body.success).toBe(true)
        })
        it('it should not get info user by provide incorrect accessToken', async () => {
            const res = await app
            .get('/api_user/info')
            .set('Authorization', 'incorrect_' + token)
            expect(res.statusCode).toBe(400);
            expect(res.body.success).toBe(false)
            expect(res.body.msg).toBe("Invalid Authentication")
        })

    })
    describe('/logout', () => {
        it('it should logout', async () => {
            const res = await app
            .get('/api_user/logout')
            expect(res.statusCode).toBe(200)
            expect(res.body.success).toBe(true)
        })
    })
    describe('/metrics', () => {
        it('should return Prometheus metrics', async () => {
            const metrics = await register.metrics();
            const res = await app
            .get('/api_user/metrics')
            .send(metrics)
            expect(res.statusCode).toBe(200);
            expect(res.headers['content-type'].split(';')[0]).toMatch(register.contentType.split(';')[0]);
        });
    });
    
    describe('/slow', () => {
        it('should return Prometheus metrics for slow', async () => {
            const res = await app.get('/api_user/slow');
            expect(res.statusCode).toBe(200);
        }, 10000);
    });
    afterAll(async () => {
        await users_coll.deleteMany({})
    })
})
