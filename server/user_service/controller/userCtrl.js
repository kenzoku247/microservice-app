const Users = require("../model/userModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const dotenv = require('dotenv')
dotenv.config()
// const { spawnSync } = require('child_process');

const Prometheus = require('prom-client');
const register = new Prometheus.Registry();
register.setDefaultLabels({
    instance: 'user_server'
})
Prometheus.collectDefaultMetrics({
  app: 'monitoring-user-server',
  timeout: 10000,
  gcDurationBuckets: [0.001, 0.01, 0.1, 1, 2, 5],
  register
})
const httpRequestTimer = new Prometheus.Histogram({
  name: 'http_request_duration_seconds',
  help: 'Duration of HTTP requests in microseconds',
  labelNames: ['method', 'route', 'code'],
  buckets: [0.1, 0.3, 0.5, 0.7, 1, 3, 5, 7, 10]
})
register.registerMetric(httpRequestTimer)

const createDelayHandler = async (req, res) => {
      if ((Math.floor(Math.random() * 100)) === 0) {
          return res.status(500).json({ msg: 'Internal Error', success: false })
      }
      const delaySeconds = Math.floor(Math.random() * (6 - 3)) + 3
      await new Promise(res => setTimeout(res, delaySeconds * 1000))
      res.end('Slow url accessed!');
};

// const getLocation = async (province,district) => {
//   try {
//       let pythonData;
//       if (province !== "" && district === "") {
//         pythonData = spawnSync('python3', ['python_script/getLocation.py', province]);
//       } else if (province !== "" && district !== ""){
//         pythonData = spawnSync('python3', ['python_script/getLocation.py', province, district]);
//       } else {
//         pythonData = spawnSync('python3', ['python_script/getLocation.py']);
//       }
//       const dataToString = pythonData.stdout.toString().trim()
//       if (dataToString !== "Error at Order Code!") {
//           const dataToJSON = JSON.parse(dataToString)
//           return dataToJSON
//       }
//   } catch (error) {
//       console.log(error)
//   }
// }

const userCtrl = {
    register: async (req, res) => {
      try {
        const { name, email, password } = req.body;
        const user = await Users.findOne({ email });
        if (user){
          return res.json({ msg: "This email already exists.", success: false });
        }
        if (password.length < 6)
          return res
            .json({ msg: "Password is at least 6 characters long.", success: false });
  
        // Password Encryption
        const passwordHash = await bcrypt.hash(password, 10);
        const newUser = new Users({
          name,
          email,
          password: passwordHash,
        });
  
        await newUser.save();
  
        const accessToken = createAccessToken({ id: newUser._id });
        const refreshToken = createRefreshToken({ id: newUser._id });
        
        
        res.cookie("refreshToken", refreshToken, {
          httpOnly: true,
          path: `/api_user/refresh_token`,
          maxAge: 7 * 24 * 60 * 60 * 1000, // 7d
        });
        
        res.json({ accessToken,success:true });
      } catch (err) {
        return res.status(500).json({ msg: err.message });
      }
    },
    login: async (req, res) => {
      try {
        const { email, password } = req.body;
        const user = await Users.findOne({ email });
        if (!user) return res.json({ msg: "User does not exist.", success: false });
  
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch){
          
          return res.json({ msg: "Incorrect password.", success: false });
        }
        const accessToken = createAccessToken({ id: user._id });
        const refreshToken = createRefreshToken({ id: user._id });
        res.cookie("refreshToken", refreshToken, {
          httpOnly: true,
          path: `/api_user/refresh_token`,
          maxAge: 7 * 24 * 60 * 60 * 1000, // 7d
        });
        
        res.json({ accessToken,success:true, msg:"Logged in successfully." });
      } catch (err) {
        return res.status(500).json({ msg: err.message });
      }
    },
    logout: async (req, res) => {
      try {
        res.clearCookie("refreshToken", { path: `/api_user/refresh_token` });
        return res.json({ msg: "Logged out", success: true });
      } catch (err) {
        return res.status(500).json({ msg: err.message });
      }
    },
    refreshToken: (req, res) => {
      try {
        const rf_token = req.cookies.refreshToken;
        if (!rf_token){
          return res.json({ msg: "Please Login or Register", success: false });
        }
  
        jwt.verify(rf_token, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
          if (err){
            return res.json({ msg: "Please Login or Register", success: false });
          }
  
          const accessToken = createAccessToken({ id: user.id });
          
          res.json({ accessToken, success: true });
        });
      } catch (err) {
        return res.status(500).json({ msg: err.message });
      }
    },
    getUser:async (req, res) => {
      try {
        const user = await Users.findById(req.user.id).select("-password");
        if (!user){
          return res.json({ msg: "User does not exist.", success: false });
        } else{
          res.json({user, success: true});
        }
      } catch (err) {
        return res.status(500).json({ msg: err.message });
      }
    },
    // location: async (req,res) => {
    //   try {
    //     const { province, district } = req.body;
    //       const provinces = await getLocation(province,district)
    //       res.json({
    //         success: true,
    //         length: provinces.length,
    //         provinces: provinces
    //     })
    //   } catch (err) {
    //     return res.status(500).json({ msg: err.message });
    //   }
    // },
    // updateUser: async (req,res) => {
    //   try {
    //     const { email, phoneNumber, password } = req.body;
    //     const user = await Users.findOne({ email })
    //     console.log(user._id)
    //   } catch (error) {
    //     return res.status(500).json({ msg: error.message });
    //   }
    // },
    // updateStore: async (req,res) => {
    //   try {
    //     const { email, storeName, storeLocation } = req.body;
    //     const user = await Users.findOneAndUpdate({ email: email }, {
    //       storeName: storeName,
    //       storeLocation: storeLocation
    //   })
    //     res.json({
    //       success: true,
    //       user:user
    //     })

    //   } catch (error) {
    //     return res.status(500).json({ msg: error.message });
    //   }
    // },
    getMetrics: async (req, res) => {
      const end = httpRequestTimer.startTimer();
      const route = req.route.path;

      res.setHeader('Content-Type',register.contentType)
      res.send(await register.metrics())
      end({ route, code: res.statusCode, method: req.method })
    },
    slow: async (req, res) => {
      const end = httpRequestTimer.startTimer();
      const route = req.route.path;

      await createDelayHandler(req, res);
      end({ route, code: res.statusCode, method: req.method })
    }
  };
  
  const createAccessToken = (user) => {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: "11m" });
  };
  const createRefreshToken = (user) => {
    return jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, { expiresIn: "7d" });
  };
  
  module.exports = userCtrl;